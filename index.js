const express = require('express');
const app = express();
const routes = require('./routes/index.js');

const port = process.env.PORT || 8080;

const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');

mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
}).then(() => {
  console.log('Successfully connected to the database');
}).catch((err) => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.get('/', (req, res) => {
  res.send('GET request to homepage');
});

app.use(express.json());
app.use('/api', routes);

app.listen(port, () => {
  console.log(`Server has been started on port ${port}`);
});
