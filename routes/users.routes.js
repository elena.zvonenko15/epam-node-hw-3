const express = require('express');
const router = new express.Router();
const UsersController = require('../controllers/users.controller.js');
const {UsersSchema} = require('../models/users.model.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyToken = require('../middlewares/verifyToken.js');
const validateSchema = require('../middlewares/validateSchema.js');

router.get('/me',
    [requestLogger, verifyToken],
    UsersController.getUser);

router.delete('/me',
    [requestLogger, verifyToken],
    UsersController.deleteUser);

router.patch('/me/password',
    [requestLogger, validateSchema(UsersSchema.changeUserPassword),
      verifyToken],
    UsersController.changeUserPassword);

module.exports = router;
