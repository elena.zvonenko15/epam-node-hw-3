const express = require('express');
const router = new express.Router();
const AuthController = require('../controllers/auth.controller.js');
const {UsersSchema} = require('../models/users.model.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyEmail = require('../middlewares/verifyEmail.js');
const validateSchema = require('../middlewares/validateSchema.js');

router.post('/register',
    [requestLogger, validateSchema(UsersSchema.createUser), verifyEmail],
    AuthController.createUser);

router.post('/login',
    [requestLogger, validateSchema(UsersSchema.loginUser)],
    AuthController.loginUser);

router.post('/forgot_password',
    [requestLogger, validateSchema(UsersSchema.forgotPassword)],
    AuthController.forgotPassword);

module.exports = router;
