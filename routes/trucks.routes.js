const express = require('express');
const router = new express.Router();
const TrucksController = require('../controllers/trucks.controller.js');
const {TrucksSchema} = require('../models/trucks.model.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyToken = require('../middlewares/verifyToken.js');
const verifyUserRole = require('../middlewares/verifyUserRole.js');
const validateSchema = require('../middlewares/validateSchema.js');

router.get('/',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    TrucksController.getTrucks);

router.post('/',
    [requestLogger, validateSchema(TrucksSchema),
      verifyToken, verifyUserRole('driver')],
    TrucksController.addTruck);

router.get('/:truckId',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    TrucksController.getTruckById);

router.put('/:truckId',
    [requestLogger, validateSchema(TrucksSchema),
      verifyToken, verifyUserRole('driver')],
    TrucksController.updateTruckById);

router.delete('/:truckId',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    TrucksController.deleteTruckById);

router.post('/:truckId/assign',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    TrucksController.assignTruckById);

module.exports = router;
