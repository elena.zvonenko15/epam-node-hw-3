const express = require('express');
const router = new express.Router();
const LoadsController = require('../controllers/loads.controller.js');
const {LoadsSchema} = require('../models/loads.model.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyToken = require('../middlewares/verifyToken.js');
const verifyUserRole = require('../middlewares/verifyUserRole.js');
const validateSchema = require('../middlewares/validateSchema.js');

router.get('/',
    [requestLogger, verifyToken],
    LoadsController.getLoads);

router.post('/',
    [requestLogger, validateSchema(LoadsSchema),
      verifyToken, verifyUserRole('shipper')],
    LoadsController.addLoad);

router.get('/active',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    LoadsController.getActiveLoad);

router.patch('/active/state',
    [requestLogger, verifyToken, verifyUserRole('driver')],
    LoadsController.iterateLoadState);

router.get('/:loadId',
    [requestLogger, verifyToken],
    LoadsController.getLoadById);

router.put('/:loadId',
    [requestLogger, verifyToken, verifyUserRole('shipper')],
    LoadsController.updateLoadById);

router.delete('/:loadId',
    [requestLogger, verifyToken, verifyUserRole('shipper')],
    LoadsController.deleteLoadById);

router.post('/:loadId/post',
    [requestLogger, verifyToken, verifyUserRole('shipper')],
    LoadsController.postLoadById);

router.get('/:loadId/shipping_info',
    [requestLogger, verifyToken, verifyUserRole('shipper')],
    LoadsController.getLoadShippingInfo);

module.exports = router;
