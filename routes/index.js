const express = require('express');
const router = new express.Router();
const authRoutes = require('./auth.routes.js');
const userRoutes = require('./users.routes.js');
const loadRoutes = require('./loads.routes.js');
const truckRoutes = require('./trucks.routes.js');

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/loads', loadRoutes);
router.use('/trucks', truckRoutes);

module.exports = router;
