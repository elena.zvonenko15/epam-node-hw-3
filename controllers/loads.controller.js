const LoadsService = require('../services/loads.service.js');
const TrucksService = require('../services/trucks.service.js');
const UsersService = require('../services/users.service.js');

class LoadsController {
  constructor() {
    this.loadStates = ['En route to Pick Up', 'Arrived to Pick Up',
      'En route to delivery', 'Arrived to delivery'];
  }

  getNextItem = (items, currentItem) => {
    const currentIndex = items.indexOf(currentItem);
    if (currentIndex !== -1) {
      const nextIndex = (currentIndex + 1) % items.length;
      return items[nextIndex];
    }
    return items[0];
  }

  getLoads = async (req, res) => {
    const user = await UsersService.getUser(req.user._id);
    const offset = req.query.offset === undefined ? 0 : +req.query.offset;
    const limit = req.query.limit === undefined ? 0 : +req.query.limit;
    const status = req.query.status === undefined ? '' : req.query.status;
    const loads = await LoadsService.getLoads(user, {status, limit, offset});
    try {
      res.send({loads});
    } catch (err) {
      res.status(500).send({
        message: 'Error retrieving loads',
      });
    }
  }

  addLoad = async (req, res) => {
    try {
      await LoadsService.addLoad(req.user._id, req.body);
      res.send({message: 'Load created successfully'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error adding load',
      });
    }
  }

  getActiveLoad = async (req, res) => {
    const load = await LoadsService.getActiveLoad(req.user._id);
    try {
      res.send({load});
    } catch (err) {
      res.status(500).send({
        message: 'Error retrieving active load',
      });
    }
  }

  iterateLoadState = async (req, res) => {
    const load = await LoadsService.getActiveLoad(req.user._id);
    try {
      if (!load) {
        return res.status(400).send({
          message: 'Active load not found',
        });
      }
      const currentLoadState = load.state;
      const newLoadState = this.getNextItem(this.loadStates, currentLoadState);
      await LoadsService.setLoadState(load._id, newLoadState);
      if (newLoadState === 'Arrived to delivery') {
        await LoadsService.setLoadStatus(load._id, 'SHIPPED');
        const truck = await TrucksService.isAssigned(load.assigned_to);
        await TrucksService.setTruckStatus(truck._id, 'IS');
      }
      const message = `Load state changed to '${newLoadState}'`;
      await LoadsService.writeLoadLog(load._id, message);
      res.send({message});
    } catch (err) {
      res.status(500).send({
        message: 'Error retrieving active load',
      });
    }
  }

  getLoadById = async (req, res) => {
    const load = await LoadsService.getLoadById(req.params.loadId);
    try {
      if (!load) {
        return res.status(400).send({
          message: `Load with id ${req.params.loadId} not found`,
        });
      }
      res.send({load});
    } catch (err) {
      return res.status(500).send({
        message: `Error retrieving load with id ${req.params.loadId}`,
      });
    }
  }

  updateLoadById = async (req, res) => {
    const load = await LoadsService.getLoadById(req.params.loadId);
    try {
      if (!load) {
        return res.status(400).send({
          message: `Load with id ${req.params.loadId} not found`,
        });
      }
      if (load.status !== 'NEW') {
        return res.status(400).send({
          message: `You can not update load with status '${load.status}'`,
        });
      }
      await LoadsService.updateLoadById(req.params.loadId, req.body);
      res.send({message: 'Load details changed successfully'});
    } catch (err) {
      return res.status(500).send({
        message: `Error updating load with id ${req.params.loadId}`,
      });
    }
  }

  deleteLoadById = async (req, res) => {
    const load = await LoadsService.getLoadById(req.params.loadId);
    try {
      if (!load) {
        return res.status(400).send({
          message: `Load with id ${req.params.loadId} not found`,
        });
      }
      if (load.status !== 'NEW') {
        return res.status(400).send({
          message: `You can not delete load with status '${load.status}'`,
        });
      }
      await LoadsService.deleteLoadById(req.params.loadId);
      res.send({message: 'Load deleted successfully'});
    } catch {
      return res.status(500).send({
        message: `Error deleting load with id ${req.params.loadId}`,
      });
    }
  }

  postLoadById = async (req, res) => {
    await LoadsService.setLoadStatus(req.params.loadId, 'POSTED');
    await LoadsService.writeLoadLog(req.params.loadId,
        `Load was set a status 'POSTED'`);
    const load = await LoadsService.getLoadById(req.params.loadId);
    const truck = await TrucksService.findTruck(load);
    try {
      if (!truck) {
        await LoadsService.setLoadStatus(req.params.loadId, 'NEW');
        await LoadsService.writeLoadLog(req.params.loadId,
            `Load was set a status 'NEW'`);
        return res.status(400).send({
          message: `Truck not found`,
        });
      }
      await LoadsService.setLoadStatus(req.params.loadId, 'ASSIGNED');
      await LoadsService.setLoadState(req.params.loadId, 'En route to Pick Up');
      await LoadsService.assignDriver(req.params.loadId, truck.assigned_to);
      await TrucksService.setTruckStatus(truck._id, 'OL');
      await LoadsService.writeLoadLog(req.params.loadId,
          `Load assigned to driver with id ${truck.assigned_to}`);
      res.send({
        message: 'Load posted successfully',
        driver_found: true,
      });
    } catch {
      return res.status(500).send({
        message: `Error posting load with id ${req.params.loadId}`,
      });
    }
  }

  getLoadShippingInfo = async (req, res) => {
    const load = await LoadsService.getLoadById(req.params.loadId);
    try {
      if (!load) {
        return res.status(400).send({
          message: `Load with id ${req.params.loadId} not found`,
        });
      }
      const truck = await TrucksService.isAssigned(load.assigned_to);
      res.send({load, truck});
    } catch (err) {
      return res.status(500).send({
        message: `Error retrieving load with id ${req.params.loadId}`,
      });
    }
  }
}

module.exports = new LoadsController();
