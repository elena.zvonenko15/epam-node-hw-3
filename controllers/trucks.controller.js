const TrucksService = require('../services/trucks.service.js');

class TrucksController {
  getTrucks = async (req, res) => {
    const trucks = await TrucksService.getTrucks(req.user._id);
    try {
      if (!trucks) {
        return res.status(400).send({
          message: 'Trucks not found',
        });
      }
      res.send({trucks});
    } catch (err) {
      res.status(500).send({
        message: 'Error retrieving trucks',
      });
    }
  }

  addTruck = async (req, res) => {
    try {
      await TrucksService.addTruck(req.user._id, req.body.type);
      res.send({message: 'Truck created successfully'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error adding truck',
      });
    }
  }

  getTruckById = async (req, res) => {
    const truck = await TrucksService.getTruckById(req.params.truckId);
    try {
      if (!truck) {
        return res.status(400).send({
          message: `Truck with id ${req.params.truckId} not found`,
        });
      }
      res.send({truck});
    } catch (err) {
      return res.status(500).send({
        message: `Error retrieving truck with id ${req.params.truckId}`,
      });
    }
  }

  updateTruckById = async (req, res) => {
    const truckID = req.params.truckId;
    const truckType = req.body.type;
    const truck = await TrucksService.updateTruckById(truckID, truckType);
    try {
      if (!truck) {
        return res.status(400).send({
          message: `Truck with id ${req.params.truckId} not found`,
        });
      }
      if (truck.status === 'OL') {
        return res.status(400).send({
          message: 'You are can not change trucks info while you are on a load',
        });
      }
      res.send({message: 'Truck details changed successfully'});
    } catch (err) {
      return res.status(500).send({
        message: `Error updating truck with id ${req.params.truckId}`,
      });
    }
  }

  deleteTruckById = async (req, res) => {
    const truck = await TrucksService.deleteTruckById(req.params.truckId);
    try {
      if (!truck) {
        return res.status(400).send({
          message: `Truck with id ${req.params.truckId} not found`,
        });
      }
      res.send({message: 'Truck deleted successfully'});
    } catch {
      return res.status(500).send({
        message: `Error deleting truck with id ${req.params.truckId}`,
      });
    }
  }

  assignTruckById = async (req, res) => {
    const isAssigned = await TrucksService.isAssigned(req.user._id);

    if (isAssigned) {
      return res.status(400).send({
        message: 'Driver is already assigned',
      });
    }

    const truckId = req.params.truckId;
    const userId = req.user._id;
    const truck = await TrucksService.assignTruckById(truckId, userId);
    try {
      if (!truck) {
        return res.status(400).send({
          message: `Truck with id ${req.params.truckId} not found`,
        });
      }
      res.send({message: 'Truck assigned successfully'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error assigned truck',
      });
    }
  }
}

module.exports = new TrucksController();
