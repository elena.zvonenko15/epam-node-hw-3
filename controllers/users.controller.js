const bcrypt = require('bcrypt');
const UsersService = require('../services/users.service.js');

class UsersController {
  isPasswordValid = (password, passwordHash) => {
    return bcrypt.compareSync(
        password,
        passwordHash,
    );
  }

  getUser = async (req, res) => {
    console.log(req.user._id);
    const user = await UsersService.getUser(req.user._id);
    try {
      if (!user) {
        return res.status(400).send({
          message: 'User not found',
        });
      }
      res.send({
        user: {
          _id: user._id,
          role: user.role,
          email: user.email,
          created_date: user.created_date,
        },

      });
    } catch (err) {
      return res.status(500).send({
        message: 'Error retrieving user',
      });
    }
  }

  deleteUser = async (req, res) => {
    const user = await UsersService.deleteUser(req.user._id);
    try {
      if (!user) {
        return res.status(400).send({
          message: 'User not found',
        });
      }
      res.send({message: 'Profile deleted successfully'});
    } catch {
      return res.status(500).send({
        message: 'Error deleting user',
      });
    }
  }

  changeUserPassword = async (req, res) => {
    if (req.body.oldPassword === req.body.newPassword) {
      return res.status(400).send({
        message: 'The new password must be different from the old one',
      });
    }

    const user = await UsersService.getUser(req.user._id);
    try {
      if (!user) {
        return res.status(400).send({
          message: 'User not found',
        });
      }

      if (!this.isPasswordValid(req.body.oldPassword, user.password)) {
        return res.status(400).send({
          message: 'Invalid old password',
        });
      }

      await UsersService.updateUserPassword(req.user._id, req.body.newPassword);
      res.send({message: 'Password changed successfully'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error updating password',
      });
    }
  }
}

module.exports = new UsersController();
