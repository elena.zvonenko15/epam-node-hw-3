const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const AuthService = require('../services/auth.service.js');
const config = require('../config/auth.config.js');

class AuthController {
  isPasswordValid = (password, passwordHash) => {
    return bcrypt.compareSync(
        password,
        passwordHash,
    );
  }

  generateToken = (userId) => jwt.sign(userId, config.secret, {expiresIn: 86400});

  createPassword = () => Math.random().toString(36).slice(-6);

  createUser = async (req, res) => {
    try {
      await AuthService.createUser(req.body);
      res.send({message: 'Profile created successfully'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error registering user',
      });
    }
  }

  loginUser = async(req, res) => {
    const user = await AuthService.getUser(req.body.email);
    try {
      if (!user) {
        return res.status(400).send({
          message: `User ${req.body.email} not found`,
        });
      }
      if (!this.isPasswordValid(req.body.password, user.password)) {
        return res.status(400).send({
          message: 'Invalid password',
        });
      }

      const jwtToken = this.generateToken({_id: user._id});

      res.send({jwt_token: jwtToken});
    } catch (err) {
      return res.status(500).send({
        message: 'Error logging user',
      });
    }
  }

   forgotPassword = async(req, res) => {
    const user = await AuthService.getUser(req.body.email);
    try {
      if (!user) {
        return res.status(400).send({
          message: `User ${req.body.email} not found`,
        });
      }
      const password = this.createPassword();
      await AuthService.changeUserPassword(req.body.email, password);
      res.send({message: 'New password sent to your email address'});
    } catch (err) {
      return res.status(500).send({
        message: 'Error updating password',
      });
    }
  }
}

module.exports = new AuthController();
