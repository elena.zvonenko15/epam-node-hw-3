const mongoose = require('mongoose');
const {LoadsModel} = require('../models/loads.model.js');

class LoadsService {
  async getLoads(user, {status, limit, offset}) {
    if (user.role === 'DRIVER') {
      return await LoadsModel
          .find({
            assigned_to: user._id,
            status: status ? status.toUpperCase() : {$ne: null},
          })
          .skip(offset)
          .limit(limit);
    } else if (user.role === 'SHIPPER') {
      return await LoadsModel
          .find({
            created_by: user._id,
            status: status ? status.toUpperCase() : {$ne: null},
          })
          .skip(offset)
          .limit(limit);
    }
  }

  async addLoad(userId, data) {
    const load = new LoadsModel({
      _id: new mongoose.Types.ObjectId().toHexString(),
      created_by: userId,
      assigned_to: '',
      status: 'NEW',
      state: '',
      logs: [],
      ...data,
    });
    return await load.save();
  }

  async getActiveLoad(userId) {
    return await LoadsModel.findOne({assigned_to: userId, status: 'ASSIGNED'});
  }

  async getLoadById(loadId) {
    return await LoadsModel.findById(loadId);
  }

  async updateLoadById(loadId, data) {
    return await LoadsModel.findByIdAndUpdate(loadId,
        {...data});
  }

  async deleteLoadById(loadId) {
    return await LoadsModel.findByIdAndRemove(loadId);
  }

  async setLoadStatus(loadId, loadStatus) {
    return await LoadsModel.findByIdAndUpdate(loadId,
        {status: loadStatus});
  }

  async setLoadState(loadId, loadState) {
    return await LoadsModel.findByIdAndUpdate(loadId,
        {state: loadState});
  }

  async writeLoadLog(loadId, logMessage) {
    const log = {
      message: logMessage,
      time: new Date(),
    };
    return await LoadsModel.findByIdAndUpdate(loadId,
        {$push: {logs: log}});
  }

  async assignDriver(loadId, driverId) {
    return await LoadsModel.findByIdAndUpdate(loadId,
        {assigned_to: driverId});
  }
}

module.exports = new LoadsService();
