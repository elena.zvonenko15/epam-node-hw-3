const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const {UsersModel} = require('../models/users.model.js');

class AuthService {
  async createUser(userData) {
    const user = new UsersModel({
      _id: new mongoose.Types.ObjectId().toHexString(),
      email: userData.email,
      password: bcrypt.hashSync(userData.password, 5),
      role: userData.role,
    });
    return await user.save();
  }

  async getUser(userEmail) {
    return await UsersModel.findOne({email: userEmail});
  }

  async changeUserPassword(userEmail, password) {
    const transporter = nodemailer.createTransport({
      host: 'mail.vs-dev.info',
      port: 465,
      auth: {
        user: 'test@vs-dev.info',
        pass: 'F@iJN&zaFx4=',
      },
    });
    const message = {
      from: 'test@vs-dev.info',
      to: userEmail,
      subject: 'Your new password',
      text: `Your new password is ${password}`,
    };
    await UsersModel.findOneAndUpdate({email: userEmail},
        {password: bcrypt.hashSync(password, 5)});
    return transporter.sendMail(message);
  }
}

module.exports = new AuthService();
