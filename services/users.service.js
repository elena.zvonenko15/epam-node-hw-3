const bcrypt = require('bcrypt');
const {UsersModel} = require('../models/users.model.js');

class UsersService {
  async getUser(userId) {
    return await UsersModel.findById(userId);
  }

  async deleteUser(userId) {
    return await UsersModel.findByIdAndRemove(userId);
  }

  async updateUserPassword(userId, userPassword) {
    return await UsersModel.findByIdAndUpdate(userId,
        {password: bcrypt.hashSync(userPassword, 5)});
  }
}

module.exports = new UsersService();
