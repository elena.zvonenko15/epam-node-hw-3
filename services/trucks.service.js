const mongoose = require('mongoose');
const {TrucksModel} = require('../models/trucks.model.js');

class TrucksService {
  constructor() {
    this.trucksTypes = {
      'sprinter': {
        payload: 1700,
        width: 300,
        length: 250,
        height: 170,
      },
      'small straight': {
        payload: 2500,
        width: 500,
        length: 250,
        height: 170,
      },
      'large straight': {
        payload: 4000,
        width: 700,
        length: 350,
        height: 200,
      },
    };
  }

  async getTrucks(userId) {
    return await TrucksModel.find({created_by: userId});
  }

  async addTruck(userId, truckType) {
    const truck = new TrucksModel({
      _id: new mongoose.Types.ObjectId().toHexString(),
      created_by: userId,
      assigned_to: '',
      type: truckType.toUpperCase(),
      status: 'IS',
    });
    return await truck.save();
  }

  async getTruckById(truckId) {
    return await TrucksModel.findById(truckId);
  }

  async updateTruckById(truckId, truckType) {
    return await TrucksModel.findByIdAndUpdate(truckId,
        {type: truckType.toUpperCase()});
  }

  async deleteTruckById(truckId) {
    return await TrucksModel.findByIdAndRemove(truckId);
  }

  async assignTruckById(truckId, userId) {
    return await TrucksModel.findByIdAndUpdate(truckId,
        {assigned_to: userId});
  }

  async isAssigned(userId) {
    return await TrucksModel.findOne({assigned_to: userId});
  }

  async findTruck(loadData) {
    const {payload, dimensions} = loadData;
    const {width, length, height} = dimensions;

    const allowedTrucks = [];

    for (const key in this.trucksTypes) {
      if (this.trucksTypes.hasOwnProperty(key)) {
        const truck = this.trucksTypes[key];
        if (truck.payload <= payload) continue;

        if (truck.width <= width) continue;

        if (truck.length <= length) continue;

        if (truck.height <= height) continue;

        allowedTrucks.push(key.toUpperCase());
      }
    }
    if (allowedTrucks.length > 0) {
      return await TrucksModel.findOne({
        assigned_to: {$ne: null},
        status: 'IS',
        type: {$in: allowedTrucks},
      });
    }
    return false;
  }

  async setTruckStatus(truckId, truckStatus) {
    return await TrucksModel.findByIdAndUpdate(truckId,
        {status: truckStatus});
  }
}

module.exports = new TrucksService();
