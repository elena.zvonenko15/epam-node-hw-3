const {UsersModel} = require('../models/users.model.js');

const verifyEmail = (req, res, next) => {
  UsersModel.findOne({
    email: req.body.email,
  }).exec((err, user) => {
    if (err) {
      return res.status(500).send({message: err});
    }
    if (user) {
      return res.status(400).send({message: 'Email is already in use'});
    }
    next();
  });
};

module.exports = verifyEmail;
