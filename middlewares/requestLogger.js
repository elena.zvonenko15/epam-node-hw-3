const fs = require('fs');
const path = require('path');

const requestLogger = (req, res, next) => {
  res.on('finish', function() {
    const message = `${req.method} ${req.originalUrl},status ${res.statusCode}`;
    console.log(message);
    fs.appendFileSync(
        `${path.dirname(require.main.filename)}/debug.log`,
        `\n${message}`);
  });
  next();
};

module.exports = requestLogger;
