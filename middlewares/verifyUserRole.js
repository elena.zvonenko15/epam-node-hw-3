const UsersService = require('../services/users.service.js');

const verifyUserRole = (requiredRole) => {
  return async (req, res, next) => {
    const user = await UsersService.getUser(req.user._id);
    if (user.role.toLowerCase() === requiredRole) {
      return next();
    } else {
      return res.status(400).send({message: 'Action not allowed'});
    }
  };
};

module.exports = verifyUserRole;
