const jwt = require('jsonwebtoken');
const config = require('../config/auth.config.js');

const verifyToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token === undefined) {
    return res.status(400).send({message: 'Unauthorized'});
  }
  jwt.verify(token, config.secret, (err, user) => {
    if (err) {
      return res.status(400).send({message: 'Invalid token'});
    }
    req.user = user;
    next();
  });
};

module.exports = verifyToken;
