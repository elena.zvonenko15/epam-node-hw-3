const mongoose = require('mongoose');
const {Schema} = mongoose;
const Joi = require('joi');

const logsSchema = new Schema({
  message: String,
  time: Date,
}, {_id: false});

const schema = new Schema(
    {
      _id: String,
      created_by: String,
      assigned_to: String,
      status: String,
      state: String,
      name: String,
      payload: Number,
      pickup_address: String,
      delivery_address: String,
      dimensions: {
        width: Number,
        length: Number,
        height: Number,
      },
      logs: [logsSchema],
    },
    {
      timestamps: {
        createdAt: 'created_date',
        updatedAt: false,
      },
      versionKey: false,
    });

const LoadsModel = mongoose.model('Load', schema);

const LoadsSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  },
});

module.exports = {
  LoadsModel,
  LoadsSchema,
};
