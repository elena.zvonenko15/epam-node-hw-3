const mongoose = require('mongoose');
const {Schema} = mongoose;
const Joi = require('joi');

const schema = new Schema(
    {
      _id: String,
      created_by: String,
      assigned_to: String,
      type: String,
      status: String,
    },
    {
      timestamps: {
        createdAt: 'created_date',
        updatedAt: false,
      },
      versionKey: false,
    });

const TrucksModel = mongoose.model('Truck', schema);

const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const TrucksSchema = Joi.object({
  type: Joi.string().valid(...truckTypes).uppercase().required(),
});

module.exports = {
  TrucksModel,
  TrucksSchema,
};
