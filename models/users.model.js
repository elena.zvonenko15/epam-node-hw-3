const mongoose = require('mongoose');
const {Schema} = mongoose;
const Joi = require('joi');

const schema = new Schema(
    {
      _id: String,
      role: String,
      email: String,
      password: String,
    },
    {
      timestamps: {
        createdAt: 'created_date',
        updatedAt: false,
      },
      versionKey: false,
    });

const UsersModel = mongoose.model('User', schema);

const UsersSchema = {
  createUser: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.string().valid('DRIVER', 'SHIPPER').uppercase().required(),
  }),
  loginUser: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }),
  forgotPassword: Joi.object({
    email: Joi.string().email().required(),
  }),
  changeUserPassword: Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required(),
  }),
};

module.exports = {
  UsersModel,
  UsersSchema,
};
